//
//  ViewController.m
//  Scoreboard
//
//  Created by Cory Dunn on 9/12/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //LeftView
    view = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor blackColor];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view];
    
    txt1 = [UITextField new];
    txt1.backgroundColor = [UIColor lightGrayColor];
    txt1.placeholder = @"Team Name";
    txt1.textAlignment = NSTextAlignmentCenter;
    txt1.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:txt1];
    
    lbl1 = [UILabel new];
    lbl1.backgroundColor = [UIColor blackColor];
    lbl1.translatesAutoresizingMaskIntoConstraints = NO;
    lbl1.text = @"0";
    lbl1.textColor = [UIColor whiteColor];
    lbl1.font = [UIFont systemFontOfSize:50];
    lbl1.textAlignment = NSTextAlignmentCenter;
    [view addSubview:lbl1];
    
    step1 = [UIStepper new];
    step1.translatesAutoresizingMaskIntoConstraints = NO;
    [step1 addTarget:self action:@selector(stepperChanged:) forControlEvents:UIControlEventValueChanged];
    step1.tintColor = [UIColor whiteColor];
    step1.maximumValue = 21;
    [view addSubview:step1];
    
    //RightView
    view1 = [[UIView alloc]initWithFrame:CGRectZero];
    view1.backgroundColor = [UIColor blackColor];
    view1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view1];
    
    txt2 = [UITextField new];
    txt2.backgroundColor = [UIColor lightGrayColor];
    txt2.placeholder = @"Team Name";
    txt2.textAlignment = NSTextAlignmentCenter;
    txt2.translatesAutoresizingMaskIntoConstraints = NO;
    [view1 addSubview:txt2];
    
    lbl2 = [UILabel new];
    lbl2.backgroundColor = [UIColor blackColor];
    lbl2.translatesAutoresizingMaskIntoConstraints = NO;
    lbl2.text = @"0";
    lbl2.textColor = [UIColor whiteColor];
    lbl2.font = [UIFont systemFontOfSize:50];
    lbl2.textAlignment = NSTextAlignmentCenter;
    [view1 addSubview:lbl2];
    
    step2 = [UIStepper new];
    step2.translatesAutoresizingMaskIntoConstraints = NO;
    [step2 addTarget:self action:@selector(stepperChanged:) forControlEvents:UIControlEventValueChanged];
    step2.tintColor = [UIColor whiteColor];
    step2.maximumValue = 21;
    [view1 addSubview:step2];
    
    //BottomView
    view2 = [[UIView alloc]initWithFrame:CGRectZero];
    view2.backgroundColor = [UIColor blackColor];
    view2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view2];
    
    restart = [UIButton new];
    restart.backgroundColor = [UIColor lightGrayColor];
    [restart setTitle:@"Start Over" forState:UIControlStateNormal];
    [restart addTarget:self action:@selector(startOver:) forControlEvents:UIControlEventTouchDown];
    restart.translatesAutoresizingMaskIntoConstraints = NO;
    [view2 addSubview:restart];
    
    //TopView
    view3 = [[UIView alloc]initWithFrame:CGRectZero];
    view3.backgroundColor = [UIColor blackColor];
    view3.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view3];
    
    title = [UILabel new];
    title.textColor = [UIColor whiteColor];
    title.textAlignment = NSTextAlignmentCenter;
    title.font = [UIFont systemFontOfSize:50];
    title.text = @"Score Keeper";
    title.translatesAutoresizingMaskIntoConstraints = NO;
    [view3 addSubview:title];
    
    
    //On-Display Positioning
    NSDictionary* variables = NSDictionaryOfVariableBindings(view, txt1, txt2, lbl1, lbl2, step1, step2,view1, view2, view3, title, restart);
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view3]|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view(view1)][view1]|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view2]|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view3]|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[view]-200-|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[view1]-200-|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-500-[view2]-0-|" options:0 metrics:nil views:variables]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view3]-550-|" options:0 metrics:nil views:variables]];
    
    [view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[txt1]|" options:0 metrics:nil views:variables]];
    [view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lbl1]|" options:0 metrics:nil views:variables]];
    [view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[step1]-50-|" options:0 metrics:nil views:variables]];
    [view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[txt1(50)]-20-[lbl1]-20-[step1]-100-|" options:0 metrics:nil views:variables]];
    
    [view1 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[txt2]|" options:0 metrics:nil views:variables]];
    [view1 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lbl2]|" options:0 metrics:nil views:variables]];
    [view1 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[step2]-50-|" options:0 metrics:nil views:variables]];
    [view1 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[txt2(50)]-20-[lbl2]-20-[step2]-100-|" options:0 metrics:nil views:variables]];
    
    [view2 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-100-[restart]-100-|" options:0 metrics:nil views:variables]];
    [view2 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-80-[restart]-80-|" options:0 metrics:nil views:variables]];
    
    [view3 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[title]-5-|" options:0 metrics:nil views:variables]];
    [view3 addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[title]-5-|" options:0 metrics:nil views:variables]];
}

-(void)stepperChanged:(UIStepper*)stepper {
    if(stepper == step1) {
        lbl1.text = [NSString stringWithFormat:@"%.f", stepper.value];
    }
    if(stepper == step2) {
        lbl2.text = [NSString stringWithFormat:@"%.f", stepper.value];
    }
}

-(void)startOver:(UIButton*)restart {
    lbl1.text = @"0";
    lbl2.text = @"0";
    step1.value = 0;
    step2.value = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// End

@end
