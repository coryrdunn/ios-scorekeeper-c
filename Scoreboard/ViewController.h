//
//  ViewController.h
//  Scoreboard
//
//  Created by Cory Dunn on 9/12/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    //VIEWS
    UIView* view;
    UIView* view1;
    UIView* view2;
    UIView* view3;
    
    //VARIABLES
    UILabel* title;
    UITextField* txt1;
    UILabel* lbl1;
    UIStepper* step1;
    UITextField* txt2;
    UILabel* lbl2;
    UIStepper* step2;
    UIButton* restart;
    
}


@end

